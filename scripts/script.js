// Завдання 1. ----------------------------------------------------------------------------------
// Створіть програму секундомір.
//             <br>
//             * Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"<br>
//             * При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий
//             * Виведення лічильників у форматі ЧЧ:ММ:СС<br>
//             * Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції

const start = document.getElementById("start"),
  stopBut = document.getElementById("stop"),
  reset = document.getElementById("reset"),
  display = document.getElementById("display"),
  hours = document.getElementById("hours"),
  minutes = document.getElementById("minutes"),
  seconds = document.getElementById("seconds");

let flag = false,
  sec = 0,
  min = 0,
  hour = 0,
  intFunc;

// let condFunc = (count1, item, count2) => {
//   if (count1 < 10) {
//     item.innerText = `0${count1}`;
//     //   count++;
//   }

//   if (count1 === 60) {
//     count1 = 0;
//     item.innerText = `0${count1}`;
//     //   count++;
//     count2++;
//   }

//   if (count1 > 9) {
//     item.innerText = `${count1}`;
//     //   count++;
//   }
// };

let timer = () => {
  intFunc = setInterval(() => {
    //   seconds ------------------------
    if (sec < 10) {
      seconds.innerText = `0${sec}`;
    }

    if (sec > 9) {
      seconds.innerText = `${sec}`;
    }

    if (sec === 60) {
      sec = 0;
      seconds.innerText = `0${sec}`;
      min++;
    }
    // condFunc(sec, seconds, min);

    // minutes ---------------------

    if (min < 10) {
      minutes.innerText = `0${min}`;
    }
    if (min > 9) {
      minutes.innerText = `${min}`;
    }

    if (min === 60) {
      min = 0;
      minutes.innerText = `0${min}`;
      hour++;
    }
    // condFunc(min, minutes, hour);

    //   hours ----------------------
    if (hour < 10) {
      hours.innerText = `0${hour}`;
    }
    if (hour > 9) {
      hours.innerText = `${hour}`;
    }

    if (hour === 60) {
      hour = 0;
      hours.innerText = `0${hour}`;
      hour++;
    }
    // condFunc(hour, hours);

    sec++;
  }, 1000);
};

start.onclick = () => {
  display.classList.remove("silver", "red");
  display.classList.add("green");
  if (!flag) {
    timer();
    flag = true;
  }
};

stopBut.onclick = () => {
  display.classList.remove("silver", "green");
  display.classList.add("red");
  clearInterval(intFunc);
  flag = false;
};

reset.onclick = () => {
  display.classList.remove("red", "green");
  display.classList.add("silver");
  clearInterval(intFunc);
  flag = false;
  sec = 0;
  min = 0;
  hour = 0;
  seconds.innerText = `0${sec}`;
  minutes.innerText = `0${min}`;
  hours.innerText = `0${hour}`;
};

// Завдання 2. ----------------------------------------------------------------------------------
// * Використовуючи JS Створіть поле для введення телефону та кнопку збереження<br>
//         * Користувач повинен ввести номер телефону у форматі 000-000-00-00<br>
//         * Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер
//         правильний
//         зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку
//         https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, відобразіть її в діві до
//         input.
const input = document.querySelector("#input"),
  save = document.querySelector("#saveButton"),
  errMsg = document.createElement("div");
errMsg.innerText = "Введіть, будь-ласка, номер у форматі 000-000-00-00";
errMsg.classList.add("red-txt");
// let testString = input.value;
// let regExp = /(\d{3}-){2}\d{2}-\d{2}/.test(`${input.value}`);

save.onclick = () => {
  let testString = input.value;
  let regExp = /(\d{3}-){2}\d{2}-\d{2}/.test(testString);
  if (regExp) {
    input.classList.add("green");
    errMsg.remove();
    setTimeout(() => {
      document.location =
        "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
    }, 3000);
  } else {
    input.before(errMsg);
  }
};
